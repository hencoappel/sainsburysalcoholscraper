import exceptions

from scrapy import Spider, Request
from sainsburys.items import SainsburysItem

class SainsburysSpider(Spider):
	category = {"wine-champagne","beer-cider","spirits-liqueurs"}
	name = "sainsburys"
	allowed_domains = ["sainsburys.co.uk"]
	drinks_url = "http://www.sainsburys.co.uk/shop/gb/groceries/drinks/"
	start_urls = [
		drinks_url+"wine-champagne",
		drinks_url+"beer-cider",
		drinks_url+"spirits-liqueurs"
	]

	def parse(self, res):
		url = res.url.split("/")
		ret_request = lambda href: Request(href.extract(), callback=self.parse)
		extract = lambda res, xpath: res.xpath(xpath).extract()[0].strip()
		if "drinks" in url and self.category & set([url[-1]]):
			aisles_sel = '//ul[@class="categories aisles"]/li/a/@href'
			for href in res.xpath(aisles_sel):
				yield ret_request(href)
		elif "drinks" in url:
			product_sel = '//div[@class="productNameAndPromotions"]/h3/a/@href'
			next_sel = '//div[@class="paginationBottom"]/ul/li[@class="next"]/a/@href'
			for href in res.xpath(product_sel):
				yield ret_request(href)
			for href in res.xpath(next_sel):
				yield ret_request(href)
		else:
			type_sel = '//li[@class="third"]/a/span/text()'
			name_sel = '//div[@class="productSummary"]/h1/text()'
			price_sel = '//p[@class="pricePerUnit"]/text()'
			percent_sel_1 = '//p[contains(text(),"Alcohol by volume:")]/text()'
			percent_sel_2 = '//span[contains(text(),"Alcohol by volume:")]/../span[2]/text()'

			item = SainsburysItem()
			item['alcohol_type'] = extract(res, type_sel)
			item['name'] = extract(res, name_sel)
			item['cost'] = extract(res, price_sel)
			try:
				item['percentage'] = extract(res, percent_sel_1)
			except exceptions.IndexError as e:
				item['percentage'] = extract(res, percent_sel_2).replace(" ", "")
			item['volume'] = item['name'].split(" ")[-1]
			yield item
