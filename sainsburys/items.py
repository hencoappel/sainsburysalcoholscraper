# -*- coding: utf-8 -*-
import scrapy

class SainsburysItem(scrapy.Item):
	name = scrapy.Field()
	alcohol_type = scrapy.Field()
	percentage = scrapy.Field()
	volume = scrapy.Field()
	cost = scrapy.Field()